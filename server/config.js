module.exports = {
  port: process.env.PORT || 3002,
  mongoURL: {
    development: process.env.MONGODB_URI || 'mongodb://localhost:27017/eleks-front-end-camp',
    test: 'mongodb://localhost:27017/node-test'
  },
  sessionSecret: 'totallysecret'
};
