const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const session = require('express-session');
const cookieParser = require('cookie-parser');

const connectMongo = require('connect-mongo');
const mongoose = require('mongoose');

const config = require('./config');
const routes = require('./routes');

const app = express();
mongoose.connect(config.mongoURL[app.settings.env]);

const MongoStore = connectMongo(session);
const sessionStore = new MongoStore({
  mongooseConnection: mongoose.connection
});

app.use(cors({ credentials: true, origin: true }));
app.use(bodyParser());
app.use(cookieParser());
app.use(session({
  secret: config.sessionSecret,
  resave: true,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    maxAge: new Date(Date.now() + 3600000),
  },
  store: sessionStore
}));

app.use(require('./middleware/loadUser'));
app.use(routes);

module.exports = {app, sessionStore, cookieParser};
