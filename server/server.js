const async = require('async');
const http = require('http');
const soketIo = require('socket.io');
const cookie = require('cookie');

const config = require('./config');

const { app, sessionStore, cookieParser } = require('./app');
const User = require('./models/user');

const httpServer = http.Server(app);
const io = soketIo(httpServer);

function loadSession(sid, callback) {
  sessionStore.get(sid, (err, session) => {
    if (arguments.length === 0) {
      return callback(null, null);
    }

    return callback(null, session)
  });
}

function loadUser(session, callback) {
  const { userId } = session;

  if (!userId) {
    return callback(null, null);
  }

  User.findById(userId, (err, user) => {
    if (err) return callback(err);

    if (!user) return callback(null, null);

    callback(null, user);
  });
}


io.use((socket, callback) => {
  const { handshake } = socket;
  async.waterfall([
    callback => {
      handshake.cookies = cookie.parse(handshake.headers.cookie || '');
      const sid = cookieParser.signedCookie(handshake.cookies['connect.sid'], config.sessionSecret);
      loadSession(sid, callback)
    },
    (session, callback) => {
      if (!session) {
        return callback('Error: No session');
      }

      handshake.session = session;
      loadUser(session, callback);
    },
    (user, callback) => {
      if (!user) {
        return callback('Error: Anonymous session');
      }

      handshake.user = user;
      callback(null);
    }
  ], err => {
    if (!err) {
      return callback(null, true);
    }

    callback(err);
  })
});

io
  .on('connection', socket => {
    const username = socket.handshake.user.get('username');

    console.error('connection');
    console.error('username', username);

    socket.broadcast.emit('join', username);

    socket.on('message', msg => {
      io.emit('message', { message: msg, username });
    });

    socket.on('disconnect', () => {
      socket.broadcast.emit('leave', username);
    });
  });

httpServer.listen(config.port, () => console.log(`Running on localhost:${config.port}`));
